import { NgModule } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'
import { ReactiveFormsModule } from '@angular/forms'
import { AppComponent } from './app.component'
import { NZ_I18N } from 'ng-zorro-antd/i18n'
import { en_US } from 'ng-zorro-antd/i18n'
import { registerLocaleData } from '@angular/common'
import en from '@angular/common/locales/en'
import { HttpClientModule } from '@angular/common/http'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { SubmissionFormComponent } from './submission-form/submission-form.component'
import { TermsFormComponent } from './terms-form/terms-form.component'
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox'
import { NzInputModule } from 'ng-zorro-antd/input'
import { NzButtonModule } from 'ng-zorro-antd/button'
import { ContactFormComponent } from './contact-form/contact-form.component'
import { NzRadioModule } from 'ng-zorro-antd/radio'
import { CountrySelectComponent } from './controls/country-select/country-select.component'
import { NzSelectModule } from 'ng-zorro-antd/select'
import { StateSelectComponent } from './controls/state-select/state-select.component'
import {
    ConfirmationPageComponent,
    RowDisplayComponent,
} from './confirmation-page/confirmation-page.component'

registerLocaleData(en)

@NgModule({
    declarations: [
        AppComponent,
        SubmissionFormComponent,
        TermsFormComponent,
        ContactFormComponent,
        CountrySelectComponent,
        StateSelectComponent,
        ConfirmationPageComponent,
        RowDisplayComponent,
    ],
    imports: [
        BrowserModule,
        ReactiveFormsModule,
        HttpClientModule,
        BrowserAnimationsModule,
        NzCheckboxModule,
        NzInputModule,
        NzButtonModule,
        NzRadioModule,
        NzSelectModule,
    ],
    providers: [{ provide: NZ_I18N, useValue: en_US }],
    bootstrap: [AppComponent],
})
export class AppModule {}

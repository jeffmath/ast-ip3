import { EventEmitter, OnInit } from '@angular/core'
import { Component, Output } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'

@Component({
    selector: 'ast-ip3-terms-form',
    templateUrl: './terms-form.component.html',
    styleUrls: ['./terms-form.component.css'],
})
export class TermsFormComponent implements OnInit {
    termsForm = this.fb.group({
        termsAccepted: [false, Validators.requiredTrue],
        signature: ['', Validators.required],
    })

    @Output() ready = new EventEmitter<FormGroup>()

    constructor(private fb: FormBuilder) {}

    ngOnInit() {
        this.ready.emit(this.termsForm)

        // for testing
        // this.termsForm.patchValue({
        //     termsAccepted: true,
        //     signature: 'a',
        // })
    }
}

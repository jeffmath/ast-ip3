import { Component, EventEmitter, OnInit, Output } from '@angular/core'
import { FormControl } from '@angular/forms'
import { countries } from '../../data/countries'

@Component({
    selector: 'ast-ip3-country-select',
    templateUrl: './country-select.component.html',
    styleUrls: ['./country-select.component.css'],
})
export class CountrySelectComponent implements OnInit {
    countryOptions = [
        { value: null, text: '' },
        ...Object.keys(countries).map((k) => ({
            value: k,
            text: countries[k],
        })),
    ]

    country = new FormControl('')

    @Output() ready = new EventEmitter<FormControl>()

    ngOnInit() {
        this.ready.emit(this.country)

        // for testing
        // this.country.setValue('US')
    }
}

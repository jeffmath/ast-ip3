import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormControl} from "@angular/forms";
import {states} from "../../data/states";

@Component({
    selector: 'ast-ip3-state-select',
    templateUrl: './state-select.component.html',
    styleUrls: ['./state-select.component.css']
})
export class StateSelectComponent implements OnInit
{
    stateOptions = [
        {value: '', text: ''},
        ...Object.keys(states).map(k => ({value: k, text: states[k]}))
    ]

    state = new FormControl('')

    @Output() ready = new EventEmitter<FormControl>();

    ngOnInit() {
        this.ready.emit(this.state);
    }
}

import { Component, EventEmitter, Output } from '@angular/core'
import { FormBuilder, FormGroup } from '@angular/forms'

@Component({
    selector: 'ast-ip3-submission-form',
    templateUrl: './submission-form.component.html',
    styleUrls: ['./submission-form.component.css'],
})
export class SubmissionFormComponent {
    submissionForm = this.fb.group({})

    @Output() finished = new EventEmitter<FormGroup>()

    constructor(private fb: FormBuilder) {}

    addTermsForm(termsForm: FormGroup) {
        this.submissionForm.addControl('termsForm', termsForm)
    }

    addContactForm(contactForm: FormGroup) {
        this.submissionForm.addControl('contactForm', contactForm)
    }

    onFinishClick() {
        if (this.submissionForm.valid) this.finished.emit(this.submissionForm)
        else this.submissionForm.markAllAsTouched()
    }
}

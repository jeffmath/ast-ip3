import { Component } from '@angular/core'
import { FormGroup } from '@angular/forms'

@Component({
    selector: 'ast-ip3-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
})
export class AppComponent {
    title = 'patent-submission-form'

    finished = false

    submissionForm?: FormGroup = undefined
}

import { EventEmitter } from '@angular/core'
import { Component, Input, Output } from '@angular/core'
import { FormGroup } from '@angular/forms'
import { NzCheckBoxOptionInterface } from 'ng-zorro-antd/checkbox/checkbox-group.component'
import { countries } from '../data/countries'
import { states } from '../data/states'

@Component({
    selector: 'ast-ip3-confirmation-page',
    templateUrl: './confirmation-page.component.html',
    styleUrls: ['./confirmation-page.component.css'],
})
export class ConfirmationPageComponent {
    @Input() submissionForm?: FormGroup

    get termsForm() {
        return this.submissionForm?.value.termsForm
    }

    get contactForm() {
        return this.submissionForm?.value.contactForm
    }

    get learnAboutMethods() {
        return this.contactForm.learnAboutMethods
            .filter((m: NzCheckBoxOptionInterface) => m.checked)
            .map((m: NzCheckBoxOptionInterface) => m.label)
    }

    get country() {
        return countries[this.contactForm.country]
    }

    get ownerCountry() {
        return countries[this.contactForm.ownerCountry]
    }

    get stateOrProvince() {
        const { country, stateOrProvince } = this.contactForm
        return country === 'US' ? states[stateOrProvince] : stateOrProvince
    }

    get ownerStateOrProvince() {
        const { ownerCountry, ownerStateOrProvince } = this.contactForm
        return ownerCountry === 'US'
            ? states[ownerStateOrProvince]
            : ownerStateOrProvince
    }

    @Output() edit = new EventEmitter()
}

@Component({
    selector: 'ast-ip3-row-display',
    template: '<div>{{label}}: <b>{{value}}</b></div>',
})
export class RowDisplayComponent {
    @Input() label = ''

    @Input() value = ''
}

import { Component, EventEmitter, OnInit, Output } from '@angular/core'
import {
    AbstractControl,
    FormBuilder,
    FormControl,
    FormGroup,
    Validators,
} from '@angular/forms'
import { NzCheckBoxOptionInterface } from 'ng-zorro-antd/checkbox/checkbox-group.component'

@Component({
    selector: 'ast-ip3-contact-form',
    templateUrl: './contact-form.component.html',
    styleUrls: ['./contact-form.component.css'],
})
export class ContactFormComponent implements OnInit {
    learnAboutOptions: NzCheckBoxOptionInterface[] = [
        { label: 'Colleague', value: 'Colleague' },
        { label: 'Conference Presentation', value: 'Conference' },
        { label: 'Email from AST', value: 'Email' },
        { label: 'Letter from AST', value: 'Letter' },
        { label: 'Linkedin mail from a colleague', value: 'LinkedIn' },
        { label: 'News or Journal Article', value: 'News' },
        { label: 'Search Engine', value: 'Search' },
        { label: 'Worked with AST in the past', value: 'WorkedWithAST' },
        { label: 'Other', value: 'Other' },
    ]

    contactForm = this.fb.group({
        role: ['Owner'],
        submitterName: ['', Validators.required],
        submitterWebsite: [''],
        firstName: ['', Validators.required],
        lastName: ['', Validators.required],
        titleOrPosition: [''],
        email: ['', [Validators.required, Validators.email]],
        phoneNumber: ['', Validators.required],
        stateOrProvince: [''],
        city: ['', Validators.required],
        postalCode: ['', Validators.required],
        ownerStateOrProvince: [''],
        ownerCity: [''],
        ownerPostalCode: [''],
        isCurrentAssignee: [true, Validators.required],
        notAssigneeExplanation: [''],
        hasPreviouslySubmitted: [true],
        learnAboutMethods: [this.learnAboutOptions],
        learnAboutOtherInfo: [''],
    })

    @Output() ready = new EventEmitter<FormGroup>()

    get isForOwner(): boolean {
        return this.contactForm.value.role === 'Owner'
    }

    get isOtherLearnAboutChecked(): boolean {
        return !!this.learnAboutOptions.find((m) => m.value === 'Other')
            ?.checked
    }

    get isCountryUS(): boolean {
        return this.contactForm.value.country === 'US'
    }

    get isOwnerCountryUS(): boolean {
        return this.contactForm.value.ownerCountry === 'US'
    }

    get email(): AbstractControl {
        return this.contactForm.controls.email
    }

    constructor(private fb: FormBuilder) {}

    ngOnInit(): void {
        // when the role selection changes, update whether the owner-address
        // controls require values
        const role = this.contactForm.get('role')
        role?.valueChanges.subscribe((value) => {
            const ownerCountry = this.contactForm.get('ownerCountry')
            const ownerCity = this.contactForm.get('ownerCity')
            const ownerPostalCode = this.contactForm.get('ownerPostalCode')
            const validators = value === 'Owner' ? null : Validators.required
            ;[ownerCountry, ownerCity, ownerPostalCode].forEach((o) => {
                o?.setValidators(validators)
                o?.updateValueAndValidity()
            })
        })

        // when the is-current-assignee selection changes, update whether
        // the associated explanation-control requires a value
        const isCurrentAssignee = this.contactForm.get('isCurrentAssignee')
        isCurrentAssignee?.valueChanges.subscribe((value) => {
            const explanation = this.contactForm.get('notAssigneeExplanation')
            if (value) explanation?.clearValidators()
            else explanation?.setValidators(Validators.required)
            explanation?.updateValueAndValidity()
        })

        this.ready.emit(this.contactForm)

        // for testing
        // this.contactForm.patchValue({
        //     submitterName: 'a',
        //     firstName: 'a',
        //     lastName: 'a',
        //     email: 'a@b',
        //     phoneNumber: 'a',
        //     city: 'a',
        //     postalCode: 'a',
        //     ownerCity: 'a',
        //     ownerPostalCode: 'a',
        // })
    }

    addCountryControl(control: FormControl) {
        control.setValidators(Validators.required)
        this.contactForm.addControl('country', control)
    }

    addStateControl(control: FormControl) {
        this.contactForm.setControl('stateOrProvince', control)
    }

    addOwnerCountryControl(control: FormControl) {
        this.contactForm.addControl('ownerCountry', control)
    }

    addOwnerStateControl(control: FormControl) {
        this.contactForm.setControl('ownerStateOrProvince', control)
    }
}
